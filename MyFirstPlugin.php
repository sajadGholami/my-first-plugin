<?php 

/*
Plugin Name: My First Plugin
Plugin URI: http://persianstack.com/plugin/
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: Sajjad GHolami
Version: 1.0.0
Author URI: http://persianstack.com/plugin/
*/



/*
function MyNameInPlugin()
{
    echo "My Name IS Sajjad";
}

function PrintUserAcount(){
    print "Acount Is Not Abilabel";
}

function OrderPurches(){
    print "This is OrderPurches";
}

add_action('add_pluguin_features','MyNameInPlugin');
add_action('add_pluguin_features','PrintUserAcount');
add_action('add_pluguin_features','OrderPurches');

do_action('add_pluguin_features');




//MyNameInPlugin("sajad GHolami");

*/

function OrderPriceProduct($price){
    return $price * 2;
}

function PriceProductOrder($price){
    return $price / 2;
}

function MinimumProduct($price){
    return $price - 1000;
}

add_filter('get_order_price','OrderPriceProduct');
add_filter('get_order_price','PriceProductOrder');
add_filter('get_order_price','MinimumProduct');

$result = apply_filters('get_order_price',25000);

echo $result; 